import { Injectable } from '@angular/core';
import {Transaction} from './transaction';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AvailabilityStatus} from './availability-status';

@Injectable({
  providedIn: 'root'
})
export class TransactionsProviderService {

  private base = 'https://localhost:5001/api/transactions';

  constructor(private http: HttpClient) {
    this.getAllTransactions();
  }

  getAllTransactions(): Observable<Transaction[]> {
    return this.http.get<Transaction[]>(`${this.base}`);
  }

  checkAvailability(transaction: Transaction, productId: number): Observable<AvailabilityStatus> {
    return this.http.post<AvailabilityStatus>(`${this.base}/check-availability/${productId}`, transaction);
  }

  addTransaction(transaction: Transaction) {
    return this.http.post(`${this.base}/add`, transaction);
  }
}

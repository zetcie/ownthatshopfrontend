import {Component, Inject, OnInit} from '@angular/core';
import {Product} from '../../product/product';
import {ProductsProviderService} from '../../product/products-provider/products-provider.service';
import {FlatTransactionDetails} from '../flat-transaction-details';
import {TransactionsProviderService} from '../transactions-provider.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Transaction} from '../transaction';
import {TransactionsMapperService} from '../transactions-mapper.service';
import {TransactionDetailsMapperService} from '../transaction-details-mapper.service';

@Component({
  selector: 'app-add-product-to-transaction',
  templateUrl: './add-product-to-transaction.component.html',
  styleUrls: ['./add-product-to-transaction.component.css']
})
export class AddProductToTransactionComponent implements OnInit {
  availableProducts: Product[];
  transactionDetails: FlatTransactionDetails;
  isRequestInProgress = false;
  errorMessage = '';

  constructor(private productsProvider: ProductsProviderService,
              private transactionsProvider: TransactionsProviderService,
              public dialogRef: MatDialogRef<AddProductToTransactionComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private transactionMapper: TransactionsMapperService,
              private transactionDetailsMapper: TransactionDetailsMapperService) { }

  ngOnInit() {
    this.availableProducts = [];
    this.transactionDetails = new FlatTransactionDetails();
    this.productsProvider.getAllProducts().subscribe(
      result => this.availableProducts = result
    );
  }

  save() {
    this.isRequestInProgress = true;
    this.errorMessage = '';
    const transaction: Transaction = this.transactionMapper.fromFlat(this.data.transaction);
    transaction.transactionDetails.push(this.transactionDetailsMapper.fromFlat(this.transactionDetails));
    this.transactionsProvider.checkAvailability(transaction, this.transactionDetails.product.id)
      .subscribe(
      result => {
        this.isRequestInProgress = false;
        if (result.isAvailable){
          this.dialogRef.close(this.transactionDetails);
        } else {
          this.errorMessage = 'Niedostępna liczba produktów. Aktualna ilość na stanie: ' + result.actualQuantity;
        }
      },
        error => {
          this.isRequestInProgress = false;
          if(error.status == 400){
            this.errorMessage = 'Liczba produktów musi być większa niż zero.'
          }else {
            this.errorMessage = 'Problemy z serwerem, proszę spróbować później.';
          }
        }
      );
  }
}

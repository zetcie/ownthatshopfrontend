import {Component, OnInit} from '@angular/core';
import {FlatTransactionDetails} from '../flat-transaction-details';
import {Client} from '../../client/client';
import {ClientsProviderService} from '../../client/clients-provider/clients-provider.service';
import {MatDialog, MatDialogRef, MatTableDataSource} from '@angular/material';
import {AddClientComponent} from '../../client/add-client/add-client.component';
import {AddProductToTransactionComponent} from '../add-product-to-transaction/add-product-to-transaction.component';
import {FlatTransaction} from '../flat-transaction';
import {TransactionsProviderService} from '../transactions-provider.service';
import {TransactionsMapperService} from '../transactions-mapper.service';
import {SelectionModel} from '@angular/cdk/collections';

@Component({
  selector: 'app-add-transaction',
  templateUrl: './add-transaction.component.html',
  styleUrls: ['./add-transaction.component.css']
})
export class AddTransactionComponent implements OnInit {
  dataSource: MatTableDataSource<FlatTransactionDetails>;
  displayedColumns = ['Select', 'Index', 'Name', 'Quantity', 'UnitPrice'];
  availableClients: Client[];
  transaction: FlatTransaction;
  isRequestInProgress = false;
  errors: Map<string,string> = new Map();
  selectedDetails = new SelectionModel<FlatTransactionDetails>(true, []);

  constructor(private clientsProvider: ClientsProviderService,
              private dialog: MatDialog,
              private transactionsProvider: TransactionsProviderService,
              public dialogRef: MatDialogRef<AddTransactionComponent>,
              private transactionMapper: TransactionsMapperService) {
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource();
    this.availableClients = [];
    this.transaction = new FlatTransaction(null);
    this.clientsProvider.getAllClients().subscribe(
      result => this.availableClients = result
    );
  }

  getTotalCost(): string {
    let totalPrice = 0;
    this.transaction.transactionDetails.forEach(
      td => totalPrice += td.quantity * td.product.price
    );
    return totalPrice.toFixed(2);
  }

  addNewClient() {
    const dialogRef = this.dialog.open(AddClientComponent, {
      height: '80vh',
      width: '80vw',
    });
  }

  addProductToTransaction() {
    const dialogRef = this.dialog.open(AddProductToTransactionComponent, {
      data: {transaction: this.transaction},
      height: '80vh',
      width: '80vw',
    });
    dialogRef.afterClosed().subscribe(
      result => {
        if (result != null && result.product != null) {
          const details: FlatTransactionDetails[] = this.dataSource.data;
          details.push(result);
          this.dataSource.data = details;
          this.transaction.transactionDetails = details;
          this.errors.delete('products')
        }
      });
  }

  submit() {
    this.errors.clear();
    this.isRequestInProgress = true;
    this.validateTransaction();
    if(this.errors.size == 0){
      this.transactionsProvider.addTransaction(this.transactionMapper.fromFlat(this.transaction)).subscribe(
        result => {},
        error => {
          if(error.status == 200){
            this.dialogRef.close(this.transaction)
          }
        }
      );
    }else{
      this.isRequestInProgress = false;
    }
  }

  validateTransaction() {
    if(this.transaction.client.id == null){
      this.errors.set('client', 'Klient musi zostać wybrany');
    }
    if(this.transaction.transactionDetails.length == 0){
      this.errors.set('products', 'Nie można dodać transakcji bez produktów');
    }
  }

  onClientChoose() {
    this.errors.delete('client')
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selectedDetails.clear() :
      this.dataSource.data.forEach(detail => {
        this.selectedDetails.select(detail);
      });
  }

  private isAllSelected() {
    const numSelected = this.selectedDetails.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected == numRows;
  }

  deleteProductFromTransaction() {
    const productsToRemove = this.selectedDetails.selected;
    this.dataSource.data = this.dataSource.data.filter(detail => !productsToRemove.includes(detail));
    this.selectedDetails.clear();
  }
}

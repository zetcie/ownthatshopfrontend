export class AvailabilityStatus {
  isAvailable: boolean;
  actualQuantity: number;
}

import {Client} from '../client/client';
import {FlatTransactionDetails} from './flat-transaction-details';

export class FlatTransaction {
  id: number;
  date: string;
  totalPrice: number;
  client: Client;
  transactionDetails: FlatTransactionDetails[];


  constructor(flatTransaction: FlatTransaction) {
    if (flatTransaction == null) {
      this.client = new Client(null);
      this.transactionDetails = [];
    } else {
      this.id = flatTransaction.id;
      this.date = flatTransaction.date;
      this.totalPrice = flatTransaction.totalPrice;
      this.client = flatTransaction.client;
      this.transactionDetails = flatTransaction.transactionDetails;
    }
  }
}

import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ProductsProviderService} from '../products-provider/products-provider.service';
import {Product} from '../product';
import {ProductType} from '../product-type';
import {FormControl, FormGroup, Validators} from '@angular/forms';


@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
  imageUrl: string;
  product: Product;
  availableProductTypes: ProductType[];
  isRequestInProgress = false;
  productForm: FormGroup;

  constructor(public dialogRef: MatDialogRef<AddProductComponent>,
              private productsProvider: ProductsProviderService) { }

  ngOnInit() {
    this.product = new Product(null);
    this.productForm = new FormGroup({
      name: new FormControl(this.product.name, [Validators.required, Validators.maxLength(100)]),
      price: new FormControl(this.product.price, [Validators.required, Validators.min(0.1)]),
      quantity: new FormControl(this.product.quantity, [Validators.required, Validators.min(1)]),
      productType: new FormControl(this.product.productType, Validators.required),
      image: new FormControl(this.product.image, Validators.required),
    });
    this.productsProvider.getAllAvailableProductTypes().subscribe(
      result => this.availableProductTypes = result
    );
  }

  loadImage() {
    this.product.image = this.imageUrl;
  }

  cancel() {
    this.dialogRef.close();
  }

  save(){
    this.isRequestInProgress = true;
    this.productsProvider.addNewProduct(this.product).subscribe(
      response => {
          console.log(response);
          this.dialogRef.close();
      },
      error => {
        this.isRequestInProgress = false;
        if(error.status == 200){
          this.dialogRef.close();
        }
      },
    );
  }
}


import {ProductType} from './product-type';

export class Product {
  constructor(product: Product) {
    if(product != null){
      this.id = product.id;
      this.image = product.image;
      this.productType = product.productType;
      this.price = product.price;
      this.quantity = product.quantity;
      this.name = product.name;
    }else {
      this.productType = new ProductType();
    }
  }

  id: number;
  name: string;
  price: number;
  quantity: number;
  image: string;
  productType: ProductType;
}

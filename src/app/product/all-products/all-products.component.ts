import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {ProductsProviderService} from '../products-provider/products-provider.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator, MatTableDataSource} from '@angular/material';
import {Product} from '../product';
import {SelectionModel} from '@angular/cdk/collections';
import {AddProductComponent} from '../add-product/add-product.component';

@Component({
  selector: 'app-all-products',
  templateUrl: './all-products.component.html',
  styleUrls: ['./all-products.component.css'],
})
export class AllProductsComponent implements OnInit {
  dataSource = new MatTableDataSource<Product>();
  columnsToDisplay = ['Select', 'Name', 'ProductType', 'Price', 'Quantity'];
  productsFetched = false;
  selectedProducts = new SelectionModel<Product>(true, []);

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private productsProvider: ProductsProviderService,
              private dialog: MatDialog) {

  }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.productsProvider.getAllProducts().subscribe(
      result => {
        this.dataSource.data = result;
        this.productsFetched = true;
      }
    );
  }

  showProductDetails(product: Product) {
    const dialogRef = this.dialog.open(ProductDetailsComponent, {
      data: {product},
      height: '40vh',
      width: '35vw',
    });

  }

  addNewProduct() {
    const dialogRef = this.dialog.open(AddProductComponent, {
      height: '40vh',
      width: '35vw',
    });

    dialogRef.afterClosed().subscribe(
      result =>
        this.productsProvider.getAllProducts().subscribe(
          products => this.dataSource.data = products
        )
    );

  }

  masterToggle() {
    this.isAllSelected() ?
      this.selectedProducts.clear() :
      this.dataSource.data.forEach(product => {
        this.selectedProducts.select(product);
      });
  }

  isAllSelected() {
    const numSelected = this.selectedProducts.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected == numRows;
  }

  delete() {
    const productsToDelete = this.selectedProducts.selected;
    this.dataSource.data = this.dataSource.data.filter(product => !productsToDelete.map(p => p.id).includes(product.id));
    this.selectedProducts.clear();
    productsToDelete.forEach(
      product => this.productsProvider.deleteProduct(product.id)
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}

@Component({
  templateUrl: './product-details.component.html',
  selector: 'app-product-details',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent {

  product: Product;
  editableProduct: Product;
  isEditModeEnabled = false;
  isRequestInProgress = false;

  constructor(public dialogRef: MatDialogRef<ProductDetailsComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private productsProvider: ProductsProviderService) {
    this.product = this.data.product;
    this.editableProduct = new Product(this.product);
  }

  edit() {
    this.isEditModeEnabled = true;
  }

  cancel() {
    this.isEditModeEnabled = false;
    this.editableProduct.name = this.product.name;
    this.editableProduct.quantity = this.product.quantity;
    this.editableProduct.price = this.product.price;
    this.editableProduct.id = this.product.id;
    this.editableProduct.image = this.product.image;
    this.editableProduct.productType = this.product.productType;
  }

  save() {
    this.isRequestInProgress = true;
    this.productsProvider.updateProduct(this.editableProduct).subscribe(
      result => console.log(),
      error => {
        this.isRequestInProgress = false;
        if (error.status == 200) {
          this.product.name = this.editableProduct.name;
          this.product.quantity = this.editableProduct.quantity;
          this.product.price = this.editableProduct.price;
          this.product.id = this.editableProduct.id;
          this.product.image = this.editableProduct.image;
          this.product.productType = this.editableProduct.productType;
          this.isEditModeEnabled = false;
        }
      }
    );
  }
}



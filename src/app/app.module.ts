import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {AllProductsComponent, ProductDetailsComponent} from './product/all-products/all-products.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MaterialModule} from './material/material.module';
import {HttpClientModule} from '@angular/common/http';
import {MAT_DIALOG_DEFAULT_OPTIONS, MatPaginatorIntl} from '@angular/material';
import { MainWindowComponent } from './main-window/main-window.component';
import {CustomPaginator} from './custom-paginator';
import { AddProductComponent } from './product/add-product/add-product.component';
import {AllClientsComponent, ClientDetailsComponent} from './client/all-clients/all-clients.component';
import { AddClientComponent } from './client/add-client/add-client.component';
import {AllTransactionsComponent, TransactionDetailsComponent} from './transaction/all-transactions/all-transactions.component';
import { AddTransactionComponent } from './transaction/add-transaction/add-transaction.component';
import { AddProductToTransactionComponent } from './transaction/add-product-to-transaction/add-product-to-transaction.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { PickFiltersComponent } from './statistics/pick-filters/pick-filters.component';

@NgModule({
  declarations: [
    AppComponent,
    AllProductsComponent,
    ProductDetailsComponent,
    MainWindowComponent,
    AddProductComponent,
    AddClientComponent,
    AllClientsComponent,
    ClientDetailsComponent,
    AllTransactionsComponent,
    TransactionDetailsComponent,
    AddTransactionComponent,
    AddProductToTransactionComponent,
    StatisticsComponent,
    PickFiltersComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule
  ],
  providers: [
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: true}},
    {provide: MatPaginatorIntl, useValue: CustomPaginator() }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ProductDetailsComponent,
    AddProductComponent,
    AddClientComponent,
    ClientDetailsComponent,
    TransactionDetailsComponent,
    AddTransactionComponent,
    AddProductToTransactionComponent,
    PickFiltersComponent,
    ]
})
export class AppModule { }

export class Contact {
  id: number;
  phoneNumber: number;
  email: string;
}

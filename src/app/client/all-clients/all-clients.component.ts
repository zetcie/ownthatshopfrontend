import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {ClientsProviderService} from '../clients-provider/clients-provider.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {AddClientComponent} from '../add-client/add-client.component';
import {Client} from '../client';
import {ClientType} from '../client-type';
import {Company} from '../company';

@Component({
  selector: 'app-all-clients',
  templateUrl: './all-clients.component.html',
  styleUrls: ['./all-clients.component.css'],
})
export class AllClientsComponent implements OnInit {
  dataSource = new MatTableDataSource<Client>();
  columnsToDisplay = ['Select', 'First Name', 'Last Name', 'City', 'Country', 'Email'];
  clientsFetched = false;
  selectedClients = new SelectionModel<Client>(true, []);

  @ViewChild(MatPaginator) paginator: MatPaginator;


  constructor(private clientsProvider: ClientsProviderService, private dialog: MatDialog) {

  }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.clientsProvider.getAllClients().subscribe(
      result => {
        this.dataSource.data = result;
        this.clientsFetched = true;
      }
    );
  }

  addNewClient() {
    const dialogRef = this.dialog.open(AddClientComponent, {
      height: '50vh',
      width: '35vw',
    });

    dialogRef.afterClosed().subscribe(
      result => this.clientsProvider.getAllClients().subscribe(
        all => this.dataSource.data = all
      )
    )
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selectedClients.clear() :
      this.dataSource.data.forEach(client => {
        this.selectedClients.select(client);
      });
  }

  isAllSelected() {
    const numSelected = this.selectedClients.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected == numRows;
  }

  delete() {
    const clientsToDelete = this.selectedClients.selected;
    this.dataSource.data = this.dataSource.data.filter(client => !clientsToDelete.map(p => p.id).includes(client.id));
    this.selectedClients.clear();
    clientsToDelete.forEach(
      client => this.clientsProvider.deleteClients(client.id)
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  showClientDetails(client: any) {
    const dialogRef = this.dialog.open(ClientDetailsComponent, {
      data: {client},
      height: '80vh',
      width: '35vw',
    });
  }
}

@Component({
  templateUrl: './client-details.component.html',
  selector: 'app-client-details',
  styleUrls: ['./client-details.component.css']
})
export class ClientDetailsComponent {

  client: Client;
  editableClient: Client;
  isEditModeEnabled = false;
  availableClientTypes: ClientType[];
  selectedClientType: number;
  availableCompanies: Company[];
  selectedCompany: number;

  constructor(public dialogRef: MatDialogRef<ClientDetailsComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private clientsProvider: ClientsProviderService) {
    this.client = this.data.client;
    this.editableClient = new Client(this.client);
    this.availableClientTypes = [];
    this.availableCompanies = [];
    clientsProvider.getAllClientTypes().subscribe(result => this.availableClientTypes = result);
    clientsProvider.getAllCompanies().subscribe(result => this.availableCompanies = result);
  }

  edit() {
    this.selectedClientType = this.client.clientType.id;
    this.selectedCompany = this.client.company.id;
    this.isEditModeEnabled = true;
  }

  cancel() {
    this.isEditModeEnabled = false;
  }

  save() {
    this.clientsProvider.updateClient(this.editableClient).subscribe(
      result => {},
      error => {
        if(error.status == 200){
          this.client = new Client(this.editableClient);
          this.isEditModeEnabled = false;
        }
      }
    );
  }
}



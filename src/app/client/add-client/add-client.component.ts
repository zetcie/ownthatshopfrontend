import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {ClientsProviderService} from '../clients-provider/clients-provider.service';
import {Client} from '../client';
import {ClientType} from '../client-type';
import {FormControl, FormGroup, Validators} from '@angular/forms';


@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.css']
})
export class AddClientComponent implements OnInit {
  client: Client;
  isRequestInProgress = false;
  availableClientTypes: ClientType[];
  clientForm: FormGroup;

  constructor(public dialogRef: MatDialogRef<AddClientComponent>,
              private clientsProvider: ClientsProviderService) {
  }

  ngOnInit() {
    this.client = new Client(null);
    this.clientForm = new FormGroup({
      firstName: new FormControl(this.client.firstName, [Validators.required, Validators.maxLength(25)]),
      surname: new FormControl(this.client.surname, [Validators.required, Validators.maxLength(25)]),
      company: new FormControl(this.client.company.name, Validators.required),
      clientType: new FormControl(this.client.clientType, Validators.required),
      email: new FormControl(this.client.contact.email, [Validators.required, Validators.email]),
      phoneNumber: new FormControl(this.client.contact.phoneNumber, [Validators.required, Validators.pattern('\\d{9}')]),
      houseNumber: new FormControl(this.client.address.houseNumber, Validators.required),
      street: new FormControl(this.client.address.street, [Validators.required, Validators.maxLength(30)]),
      city: new FormControl(this.client.address.city, [Validators.required, Validators.maxLength(30)]),
      country: new FormControl(this.client.address.country, [Validators.required, Validators.maxLength(30)]),
    });
    this.clientsProvider.getAllClientTypes().subscribe(
      result => this.availableClientTypes = result
    );
  }

  cancel() {
    this.dialogRef.close();
  }

  save() {
    this.client.firstName = this.clientForm.get('firstName').value;
    this.client.surname = this.clientForm.get('surname').value;
    this.client.company.name = this.clientForm.get('company').value;
    this.client.clientType = this.clientForm.get('clientType').value;
    this.client.contact.email = this.clientForm.get('email').value;
    this.client.contact.phoneNumber = this.clientForm.get('phoneNumber').value;
    this.client.address.houseNumber = this.clientForm.get('houseNumber').value;
    this.client.address.street = this.clientForm.get('street').value;
    this.client.address.city = this.clientForm.get('city').value;
    this.client.address.country = this.clientForm.get('country').value;
    this.isRequestInProgress = true;
    this.clientsProvider.addNewClient(this.client).subscribe(
      result => console.log(result),
      error => {
        this.isRequestInProgress = false;
        console.log(error);
        if (error.status == 200) {
          this.dialogRef.close();
        }
      }
    );
  }
}

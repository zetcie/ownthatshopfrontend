import { TestBed } from '@angular/core/testing';

import { ClientsProviderService } from './clients-provider.service';

describe('ClientsProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClientsProviderService = TestBed.get(ClientsProviderService);
    expect(service).toBeTruthy();
  });
});

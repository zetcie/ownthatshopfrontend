import {Address} from './address';
import {Contact} from './contact';
import {ClientType} from './client-type';
import {Company} from './company';

export class Client {
    constructor(client: Client) {
      if(client != null){
        this.id = client.id;
        this.firstName = client.firstName;
        this.surname = client.surname;
        this.company = client.company;
        this.address = client.address;
        this.contact = client.contact;
        this.clientType = client.clientType;
      } else {
        this.address = new Address();
        this.contact = new Contact();
        this.clientType = new ClientType();
        this.company = new Company();
      }
    }
    id: number;
    firstName: string;
    surname: string;
    company: Company;
    address: Address;
    contact: Contact;
    clientType: ClientType;
  }

import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {FiltersDto} from './filters-dto';

@Injectable({
  providedIn: 'root'
})
export class StatisticsProviderService {

  private base = 'https://localhost:5001/api/statistics';

  constructor(private http: HttpClient) { }

  getResult(result: string, filters: FiltersDto) {
    return this.http.post(`${this.base}/${result}`, filters);
  }
}

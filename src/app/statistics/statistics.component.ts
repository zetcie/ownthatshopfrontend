import { Component, OnInit } from '@angular/core';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {StatisticsProviderService} from './statistics-provider.service';
import {TransactionsMapperService} from '../transaction/transactions-mapper.service';
import {PickFiltersComponent} from './pick-filters/pick-filters.component';
import {Filters} from './filters';
import {FiltersDto} from './filters-dto';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {
  dataSource = new MatTableDataSource();
  columnsToDisplay = [];
  productColumns = ['Name', 'ProductType', 'Price', 'Quantity'];
  clientColumns = ['First Name', 'Last Name', 'City', 'Country', 'Email'];
  transactionColumns = ['Id', 'Company', 'FirstName', 'Surname', 'TotalPrice'];
  desiredResult = "products";
  filters = new Filters();

  constructor(private statisticsProvider: StatisticsProviderService,
              private mapper: TransactionsMapperService,
              private dialog: MatDialog) {
    this.columnsToDisplay = this.productColumns;
  }

  ngOnInit() {
  }

  changeResult() {
    this.dataSource.data = [];
    switch(this.desiredResult){
      case "products":
        this.columnsToDisplay = this.productColumns;
        break;
      case "clients":
        this.columnsToDisplay = this.clientColumns;
        break;
      case "transactions":
        this.columnsToDisplay = this.transactionColumns;
        break;
    }
  }

  getResults() {
    this.statisticsProvider.getResult(this.desiredResult, FiltersDto.of(this.filters)).subscribe(
      result =>{
        var entities = result['queryResult'].map(e => e['entity']);
        if(this.desiredResult == "transactions"){
          entities = entities.map(e => this.mapper.toFlat(e))
        }
        this.dataSource.data = entities;
      }
    )
  }

  pickFilters() {
    const filters = this.filters;
    const dialogRef = this.dialog.open(PickFiltersComponent, {
      data: {filters},
      height: '60vh',
      width: '30vw',
    });

    dialogRef.afterClosed().subscribe(
      result => {
        if(result != null){
          this.filters = result;
        }
      }
    )
  }

  clearFilters() {
    this.filters = new Filters();
  }
}

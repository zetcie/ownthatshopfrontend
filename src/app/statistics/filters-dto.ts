import {Filters} from './filters';

// const filtersNames = ['Country', 'ClientType', 'DateRange', 'PriceRange', 'Product', 'Client'];
export class FiltersDto {
  filterConfigurations = {};

  private constructor() {

  }

  static of(
    filters: Filters
  ): FiltersDto {

    const filtersDto = new FiltersDto();

    if (filters.country != null) {
      filtersDto.filterConfigurations['Country'] = {
        'parameters': {
          'countryName': filters.country
        }
      };
    }

    if (filters.clientType != null) {
      filtersDto.filterConfigurations['ClientType'] = {
        'parameters': {
          'name': filters.clientType.name
        }
      };
    }

    if (filters.client != null) {
      filtersDto.filterConfigurations['Client'] = {
        'parameters': {
          'id': filters.client.id
        }
      };
    }

    if (filters.product != null) {
      filtersDto.filterConfigurations['Product'] = {
        'parameters': {
          'id': filters.product.id
        }
      };
    }

    if (filters.startDate != null) {
      filtersDto.filterConfigurations['DateRange'] = {
        'parameters': {
          'start': filters.startDate
        }
      };
    }

    if (filters.endDate != null) {
      var dateRange = filtersDto.filterConfigurations['DateRange'];
      if (dateRange != null) {
        dateRange['parameters']['end'] = filters.endDate;
      } else {
        filtersDto.filterConfigurations['DateRange'] = {
          'parameters': {
            'end': filters.endPrice
          }
        };
      }
    }

    if (filters.startPrice != null) {
      filtersDto.filterConfigurations['PriceRange'] = {
        'parameters': {
          'start': filters.startPrice
        }
      };
    }

    if (filters.endPrice != null) {
      const priceRange = filtersDto.filterConfigurations['PriceRange'];
      if (priceRange != null) {
        priceRange['parameters']['end'] = filters.endPrice;
      } else {
        filtersDto.filterConfigurations['PriceRange'] = {
          'parameters': {
            'end': filters.endPrice
          }
        };
      }
    }

    console.log(filtersDto);

    return filtersDto;
  }
}

import {Client} from '../client/client';
import {Product} from '../product/product';
import {ClientType} from '../client/client-type';

export class Filters {
  country: string;
  startDate: Date;
  endDate: Date;
  clientType: ClientType;
  startPrice: number;
  endPrice: number;
  client: Client;
  product: Product;

  count(): number{
    var total = 0;
    total += this.country == null ? 0 : 1;
    total += this.startDate == null ? 0 : 1;
    total += this.endDate == null ? 0 : 1;
    total += this.clientType == null ? 0 : 1;
    total += this.startPrice == null ? 0 : 1;
    total += this.endPrice == null ? 0 : 1;
    total += this.client == null ? 0 : 1;
    total += this.product == null ? 0 : 1;
    return total;
  }
}
